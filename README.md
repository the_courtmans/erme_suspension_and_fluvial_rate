# River Erme Fluvial Rate and Suspension Analysis

## Usage of Repository
This is the parent repository for all projects investigating the River Erme. New projects should be be make inside this repo.

New projects should exist in the tree thus:
```
erme_suspension_and_fluivial_rate/
--new_project/
  --README.md
  --other_project_files.txt
  --project_package/
    --etc.csv
```

To make changes use a feature branch `feature/new_fancy_thing` and merge to main via a PR.

### Commit Strings
Ideally commit strings should start with the following prefixes:
* INIT: Changes to repo structure/Makefiles/.gitignores etc
* DOCS: Updates to READMEs, notes, and design images
* LINT: Spelling or syntax changes
* TEST: If you've just written test code
* FUNC: Functional change to code (can also fold in TEST, REFACTOR and DOCS if they are small)
* REFACTOR: Making code clean and DRY without changing functionality
* CICD: Changes to pipelines
* ANALYSIS: Data exploration and analysis


## Hypotheses
Can the data be used to assess the plausibility of several inter-linked hypotheses:

1. A combination of over-grazing of the moor was compounded by the dry summers of 1975 and 1976, plus some long term effects from linear structures high on the eastern side of the catchment, to bring about a general drying and compaction of the peat and peaty soils in the upper Erme catchment.  This occurred really from about 1970 to 1990.
2. This has led to a faster flood response time because of lower water retention. The floods occur more quickly and are more turbid – i.e. they carry more sediment or suspended organic matter.  This still seems to be the case.
3. By 1995 there were agri-environmental schemes in place that limited grazing on the moor and has allowed vegetation to recover (though apparently not yet the water retention capacity of the peat and soil).  These schemes have limited the cattle allowed to be out-wintered on the moor and required that they be brought into the inbye (enclosed farmland).  In turn this has led to more soil poaching and greater erosion from the inbye fields, feeding into the tributaries.

These data may not be frequent enough to show trends, but it would be interesting to see if any long term variations can be picked up in aspects such as suspended solids, nitrates and biological oxygen demand (BOD).  Plus variability between the two locations.
