# Coding Club Discussion
> Stuff wot Mike said

## General Fixes and Improvements
* We want to be able to see df.info() in full
* When creating new dfs specify index column name to avoid new index being created
* DynamoDB table to store Measurement units and related info

## Further Things
* Analyse what data looks like (ie. missing data chunks, do latter years have more measurements?)

## Visualisation
* Covariance
* Distribution plots (density plots)
