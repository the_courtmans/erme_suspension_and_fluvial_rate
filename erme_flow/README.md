# Erme Flow
> Support package for the Erme Suspension and Fluvial Rate project.

## Get Started
To install run the following from inside this directory:
```bash
make
make install
```

## Components
### Consolidator Class
This interface class links together the DataFrame Consolidation class, with the S3 upload and retrieval class and the DynamoDB Site Data class and is exposed as a simple importable interface. The workflow goes as:

1. Retrieve raw data from S3
2. Consolidate data:
   1. Split DataFrame
   2. Extract site metadata
   3. Pivot readings into columns
3. Upload site metadata to DynamoDB
4. Upload DataFrames to S3 as CSVs

Augmented DataFrames (Objects with DataFrames and a metadata dictionary) are available as class attributes in a dictionary.

```python3
from erme_flow import Consolidator, ConsolidationKeys, ConsolidationBuckets, \
    ConsolidationDynamoDB

# Set up consolidator class inputs
keys = ConsolidationKeys(
    splitting="NAME",
    normalisation=[
        "Date",
        "Time",
        "DoW",
    ],
    measured_entity="DETNAME",
    result="RESULT",
)
buckets = ConsolidationBuckets(
    raw_data="rawdata-bucket",
    processed_data="processeddata-bucket",
)
table_details = ConsolidationDynamoDB(
    table="sitedata-dynamodb-table",
    partition_key="Site Name",
    primary_key_in_data="NAME",
)

# Create consolidator class
consolidator = Consolidator(
    buckets=buckets,
    table_details=table_details,
    consolidation_keys=keys,
)

# Run consolidation
consolidator.run()
```


## Design Notes
![Code](../img/code.png "Code")

