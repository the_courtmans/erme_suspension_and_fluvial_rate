"""Erme Suspension and Fluvial Rate package."""
from erme_flow.interface.raw_data_consolidator import \
    RawDataConsolidator as Consolidator
from erme_flow.model import ConsolidationKeys
from erme_flow.model import RawDataConsolidatorBuckets as ConsolidationBuckets
from erme_flow.model import \
    RawDataConsolidatorTableDetails as ConsolidationDynamoDB
