"""Module to hold Data Collection class."""
from io import StringIO
from typing import List

import boto3
import pandas as pd
from mypy_boto3_s3 import S3Client, S3ServiceResource
from mypy_boto3_s3.service_resource import Bucket, ObjectSummary
from mypy_boto3_s3.type_defs import GetObjectOutputTypeDef


class DataCollection:
    """Class to store and retrieve dataframes as CSVs on S3.

    Parameters
    ----------
    bucket (str)       S3 bucket name - protocol not required
    file_name (str)    Remote file path (including directories if applicable)

    Attributes
    ----------
    df (pd.DataFrame)  Dataframe retrieved or for upload

    Methods
    -------
    retrieve           Obtain data from S3 and return as dataframe
    upload             Upload dataframe as CSV to S3
    """

    df: pd.DataFrame
    _bucket_name: str
    _file_name: str
    _s3: S3ServiceResource
    _bucket: Bucket
    _s3_client: S3Client
    _s3_object: GetObjectOutputTypeDef

    def __init__(self, bucket: str, file_name: str) -> None:
        """Initialise with bucket and remote file name."""
        self._bucket_name = bucket
        self._file_name = file_name
        self._s3 = boto3.resource("s3")

        self._validate_class_inputs()

        self._bucket = self._s3.Bucket(self._bucket_name)

    def _validate_class_inputs(self) -> None:
        self._validate_file_is_specified()
        self._validate_s3_bucket_is_specified()
        self._validate_s3_bucket_exists()

    def _validate_s3_bucket_is_specified(self) -> None:
        if not self._bucket_name:
            raise ValueError("No S3 bucket specified.")

    def _validate_s3_bucket_exists(self) -> None:
        if self._s3.Bucket(self._bucket_name) not in self._s3.buckets.all():
            raise ValueError("Bucket does not exist.")

    def _validate_file_is_specified(self) -> None:
        if not self._file_name:
            raise ValueError("No file name specified.")

    def retrieve(self) -> pd.DataFrame:
        """Obtain data from S3 and return as dataframe."""
        self._check_file_exists_in_bucket()
        self._retrieve_object_from_bucket()
        self._convert_object_to_dataframe()

        return self.df

    def _check_file_exists_in_bucket(self) -> None:
        objects: List[ObjectSummary]
        objects = list(self._bucket.objects.filter(Prefix=self._file_name))

        if not any([item.key == self._file_name for item in objects]):
            raise ValueError("File not in bucket.")

    def _retrieve_object_from_bucket(self) -> None:
        self._s3_client = boto3.client("s3")
        self._s3_object = self._s3_client.get_object(
            Bucket=self._bucket_name,
            Key=self._file_name,
        )

    def _convert_object_to_dataframe(self) -> None:
        self.df = pd.read_csv(self._s3_object["Body"])

    def upload(self, df) -> None:
        """Upload DataFrame to S3."""
        self.df = df
        csv_buffer: StringIO = StringIO()
        self.df.to_csv(csv_buffer)

        self._s3.Object(
            self._bucket_name,
            self._file_name
        ).put(Body=csv_buffer.getvalue())
