"""Module to hold Site Data Uploader class."""
from typing import Any, Dict, List

import boto3
import numpy as np


class SiteDataUploader:
    """Class for uploading site data to DynamoDB.

    Parameters
    ----------
    table (dictionary)  [table, partition_key, primary_key_in_data]
    data (dictionary)   Data to be stored in DynamoDB

    Methods
    -------
    upload              Upload data to DynamoDB
    """

    _table: Dict[str, str]
    _data: Dict[str, Any]

    def __init__(
            self,
            table: Dict[str, str],
            data: Dict[str, Any],
    ) -> None:
        """Initialise with table and data dictionaries."""
        self._table = table
        self._data = data

        self._validate_class_inputs()
        self._convert_nan_to_zero()
        self._convert_int64_to_int()

    def _validate_class_inputs(self) -> None:
        self._validate_data_dictionary_is_not_empty()
        self._validate_table_dictionary_has_all_key_values()
        self._validate_primary_key_specified_exists_in_data()

    def _validate_data_dictionary_is_not_empty(self) -> None:
        if not self._data:
            raise ValueError("No data provided for upload.")

    def _validate_table_dictionary_has_all_key_values(self) -> None:
        expected_keys: List[str] = [
            "table",
            "partition_key",
            "primary_key_in_data",
        ]

        for key in expected_keys:
            if key not in self._table:
                raise ValueError(
                    "Key table values not provided for DynamoDB."
                )

    def _validate_primary_key_specified_exists_in_data(self) -> None:
        if self._table["primary_key_in_data"] not in self._data:
            raise ValueError("Primary key not found in data.")

    def _convert_nan_to_zero(self) -> None:
        for heading, value in self._data.items():
            if value != value:
                self._data[heading] = int(0)

    def _convert_int64_to_int(self) -> None:
        for heading, value in self._data.items():
            if isinstance(value, np.int64):
                self._data[heading] = value.item()

    def upload(self) -> None:
        """Upload data to table."""
        self._create_dynamodb_connection()
        self._parse_data_for_uploading()
        self._add_in_partition_key()
        self._upload_data()

    def _create_dynamodb_connection(self) -> None:
        connection = boto3.resource("dynamodb")
        self._dynamodb_table = connection.Table(self._table["table"])

    def _parse_data_for_uploading(self) -> None:
        self._data_for_upload = self._data

    def _add_in_partition_key(self) -> None:
        self._data_for_upload[self._table["partition_key"]] = \
            self._data[self._table["primary_key_in_data"]]

    def _upload_data(self) -> None:
        self._dynamodb_table.put_item(
            Item=self._data_for_upload,
        )
