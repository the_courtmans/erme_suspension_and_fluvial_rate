"""Module to hold Raw Data Consolidator interface class."""
from typing import Dict, List

import pandas as pd

from erme_flow.connection.data_collection import DataCollection
from erme_flow.connection.site_data_uploader import SiteDataUploader
from erme_flow.model import AugmentedDF, ConsolidationKeys
from erme_flow.model import RawDataConsolidatorBuckets as Buckets
from erme_flow.model import RawDataConsolidatorTableDetails as TableDetails
from erme_flow.service.dataframe_consolidator import DFConsolidator


class RawDataConsolidator:
    """Interface class for coordinating the raw data consolidation.

    Parameters
    ----------
    buckets (Buckets)                       Raw and processed bucket strings
    table_details (TableDetails)            Table details for DynamoDB upload
    consolidation_keys (ConsolidationKeys)  Keys on which to sort the data

    Attributes
    ----------
    raw (pd.DataFrame)                      Raw dataframe
    processed (Dict)                        List of tidied DataFrames

    Methods
    -------
    run                                     Retrieve, process, and upload data
    """

    raw: pd.DataFrame
    processed: Dict[str, AugmentedDF]
    _buckets: Buckets
    _table_details: TableDetails
    _consolidation_keys: ConsolidationKeys
    _site_name: str
    _site_frame: AugmentedDF

    def __init__(
            self,
            buckets: Buckets,
            table_details: TableDetails,
            consolidation_keys: ConsolidationKeys
    ):
        """Initialise interface class with buckets and table details."""
        self._buckets = buckets
        self._table_details = table_details
        self._consolidation_keys = consolidation_keys

        self._validate_inputs()

    def _validate_inputs(self) -> None:
        self._validate_bucket_strings_are_not_empty()
        self._validate_table_details_are_not_empty()

    def _validate_bucket_strings_are_not_empty(self) -> None:
        for attribute in self._buckets:
            if not attribute:
                raise ValueError("Bucket list contains empty values.")

    def _validate_table_details_are_not_empty(self) -> None:
        for attribute in self._table_details:
            if not attribute:
                raise ValueError("Table Details contain empty values.")

    def run(self) -> Dict[str, AugmentedDF]:
        """Run all steps to consolidate data and handle AWS synchronisation."""
        self._retrieve_data_from_bucket()
        self._consolidate_dataframe()

        for self._site_name, self._site_frame in self.processed.items():
            self._upload_site_data_to_dynamodb()
            self._upload_data_to_bucket()

        return self.processed

    def _retrieve_data_from_bucket(self) -> None:
        raw_collection = DataCollection(
            bucket=self._buckets.raw_data,
            file_name="raw.csv",
        )
        raw_collection.retrieve()
        self.raw = raw_collection.df

    def _consolidate_dataframe(self) -> None:
        consolidator = DFConsolidator(
            df=self.raw,
            keys=self._consolidation_keys
        )
        consolidator.consolidate()
        self.processed = consolidator.processed

    def _upload_site_data_to_dynamodb(self) -> None:
        table_data: Dict[str, str] = {
            "table": self._table_details.table,
            "partition_key": self._table_details.partition_key,
            "primary_key_in_data": self._table_details.primary_key_in_data,
        }
        site_data_uploader = SiteDataUploader(
            table=table_data,
            data=self._site_frame.id,
        )

        site_data_uploader.upload()

    def _upload_data_to_bucket(self) -> None:
        file_name: str = f"{self._site_name.replace(' ', '_').lower()}.csv"
        processed_collection = DataCollection(
            bucket=self._buckets.processed_data,
            file_name=file_name,
        )
        processed_collection.upload(
            df=self._site_frame.df,
        )
