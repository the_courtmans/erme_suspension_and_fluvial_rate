"""Module for storing model classes."""
from dataclasses import dataclass
from typing import Any, Dict, List, NamedTuple

import pandas as pd


@dataclass
class AugmentedDF:
    """Object to store DataFrames and location identifying information."""

    id: Dict[str, Any]
    df: pd.DataFrame


@dataclass
class ConsolidationKeys:
    """Object to store keys with which to consolidate DataFrames."""

    splitting: str
    normalisation: List[str]
    measured_entity: str
    result: str


class RawDataConsolidatorBuckets(NamedTuple):
    """Object to store class inputs for the RawDataConsolidator class."""

    raw_data: str
    processed_data: str


class RawDataConsolidatorTableDetails(NamedTuple):
    """Object to store class inputs for the RawDataConsolidator class."""

    table: str
    partition_key: str
    primary_key_in_data: str
