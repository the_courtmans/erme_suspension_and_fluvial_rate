"""Module to hold DataFrame Consolidator class.

A facade service class for the DataFrame tidying classes:
* DataFrame Splitter
* DataFrame Identity Extractor
* DataFrame Normaliser
"""
from typing import Dict, List

import pandas as pd

from erme_flow.model import AugmentedDF, ConsolidationKeys
from erme_flow.service.dataframe_identity_extractor import DFIDExtractor
from erme_flow.service.dataframe_normaliser import DFNormaliser
from erme_flow.service.dataframe_splitter import DFSplitter


class DFConsolidator:
    """Facade class to consolidate raw data DataFrames.

    Parameters
    ----------
    df (pd.DataFrame)         The raw DataFrame
    keys (ConsolidationKeys)  Keys object for each step

    Attributes
    ----------
    original (pd.DataFrame)   Original DataFrame provided
    processed (dictionary)    Dictionary of AugmentedDFs

    Methods
    -------
    consolidate               Split, extract identities from, and normalise
    """

    original: pd.DataFrame
    processed: Dict[str, AugmentedDF]
    _keys: ConsolidationKeys
    _keys_for_testing: List[str]
    _split_frames: List[pd.DataFrame]
    _augmented_frames: List[AugmentedDF]

    def __init__(self, df: pd.DataFrame, keys: ConsolidationKeys) -> None:
        """Initialise class with df and consolidation keys."""
        self.original = df
        self._keys = keys

        self._validate_class_inputs()

    def _validate_class_inputs(self) -> None:
        self._validate_dataframe_has_enough_rows()
        self._validate_keys_object_is_not_empty()
        self._validate_keys_are_in_dataframe()

    def _validate_dataframe_has_enough_rows(self) -> None:
        if self.original.shape[0] < 2:
            raise ValueError("DataFrame has too few rows.")

    def _validate_keys_object_is_not_empty(self) -> None:
        if not (
            self._keys.splitting or
            self._keys.normalisation or
            self._keys.measured_entity or
            self._keys.result
        ):
            raise ValueError("Keys object contains empty values.")

    def _validate_keys_are_in_dataframe(self) -> None:
        self._flatten_keys_object_for_testing()
        self._check_for_keys_in_dataframe()

    def _flatten_keys_object_for_testing(self) -> None:
        self._keys_for_testing: List[str] = []
        self._keys_for_testing.append(self._keys.splitting)
        self._keys_for_testing += self._keys.normalisation
        self._keys_for_testing.append(self._keys.measured_entity)
        self._keys_for_testing.append(self._keys.result)

    def _check_for_keys_in_dataframe(self) -> None:
        for key in self._keys_for_testing:
            if key not in self.original.columns:
                raise ValueError("Keys do not exist in DataFrame.")

    def consolidate(self) -> None:
        """Split, extract identities from, and normalise the DataFrame."""
        self._split()
        self._extract_identities()
        self._normalise_and_publish()

    def _split(self) -> None:
        splitter: DFSplitter = DFSplitter(
            src_df=self.original,
            key=self._keys.splitting,
        )
        self._split_frames = splitter.split()

    def _extract_identities(self) -> None:
        self._augmented_frames = []
        id_extractor: DFIDExtractor

        for df in self._split_frames:
            id_extractor = DFIDExtractor(
                df=df,
            )
            id_extractor.extract()
            self._augmented_frames.append(id_extractor.processed)

    def _normalise_and_publish(self) -> None:
        self.processed = {}
        normaliser: DFNormaliser

        for augmented_df in self._augmented_frames:
            self._run_normalisation(augmented_df)
            self._publish_normalised_augmented_dataframes(augmented_df)

    def _run_normalisation(self, augmented_df: AugmentedDF) -> None:
        normaliser = DFNormaliser(
            df=augmented_df.df,
            keys=self._keys.normalisation,
            measurement_name=self._keys.measured_entity,
            result=self._keys.result,
        )
        normaliser.normalise()
        self._normalised_frame = normaliser.normalised

    def _publish_normalised_augmented_dataframes(
            self,
            augmented_df: AugmentedDF
    ) -> None:
        name: str = augmented_df.id[self._keys.splitting]
        new_augmented_df: AugmentedDF = AugmentedDF(
            id=augmented_df.id,
            df=self._normalised_frame,
        )
        self.processed[name] = new_augmented_df
