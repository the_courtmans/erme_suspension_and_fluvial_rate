"""Module to hold DataFrame Identity Extractor class."""
from typing import Any, Dict, List

import pandas as pd

from erme_flow.model import AugmentedDF


class DFIDExtractor:
    """Class to extract common column values as metadata.

    Parameters
    ----------
    df (pd.DataFrame)        DataFrame to be normalised

    Attributes
    ----------
    original (pd.DataFrame)  Original DataFrame provided
    processed (AugmentedDF)  Split out Dataframes

    Methods
    -------
    extract                  Extract metadata and trim DataFrame
    """

    original: pd.DataFrame
    processed: AugmentedDF
    _id_columns: List[str]
    _id: Dict[str, Any]
    _working_df: pd.DataFrame

    def __init__(self, df: pd.DataFrame) -> None:
        """Initialise class and validate inputs."""
        self.original = df
        self._validate_class_input()

    def _validate_class_input(self) -> None:
        self._validate_columns_are_not_all_singly_valued()
        self._validate_some_columns_are_singly_valued()

    def _validate_columns_are_not_all_singly_valued(self) -> None:
        a_column_has_varying_data: bool = False

        for column in self.original:
            if self.original[column].nunique() > 1:
                a_column_has_varying_data = True

        if not a_column_has_varying_data:
            raise ValueError("All columns contain single values.")

    def _validate_some_columns_are_singly_valued(self) -> None:
        all_columns_have_varying_data: bool = True

        for column in self.original:
            if self.original[column].nunique() == 1:
                all_columns_have_varying_data = False

        if all_columns_have_varying_data:
            raise ValueError("No column contains single values.")

    def extract(self) -> AugmentedDF:
        """Extract metadata and trim dataframe."""
        self._extract_metadata()
        self._trim_metadata_from_dataframe()
        self._create_augmented_dataframe()

        return self.processed

    def _extract_metadata(self) -> None:
        self._find_singly_valued_columns()
        self._extract_values_from_singly_valued_columns()

    def _find_singly_valued_columns(self) -> None:
        self._id_columns = []
        for column in self.original:
            if self.original[column].nunique() == 1:
                self._id_columns.append(column)

    def _extract_values_from_singly_valued_columns(self) -> None:
        self._id = {}
        for column in self._id_columns:
            self._id[column] = self.original[column].iloc[0]

    def _trim_metadata_from_dataframe(self) -> None:
        self._working_df = self.original

        for column in self._id_columns:
            self._working_df.drop(columns=column, inplace=True)

    def _create_augmented_dataframe(self) -> None:
        self.processed = AugmentedDF(
            id=self._id,
            df=self._working_df,
        )
