"""Module to hold DataFrame Normaliser class."""
from typing import Any, Dict, List, Tuple

import numpy as np
import pandas as pd


class DFNormaliser:
    """Class for normalising dataframes.

    Parameters
    ----------
    df (pd.DataFrame)           DataFrame to be normalised
    keys (List[str])            Keys upon which to normalise
    measurement_name (str)      Column with measurement name
    result (str)                Column with result

    Attributes
    ----------
    original (pd.DataFrame)    Original DataFrame provided
    normalised (pd.DataFrame)  Split out Dataframes

    Methods
    -------
    normalise                  Separate the DataFrame according to key
    """

    original: pd.DataFrame
    _keys: List[str]
    _measurement_name: str
    _result: str
    _key_combinations: List[Tuple[Any]]

    def __init__(
        self,
        df: pd.DataFrame,
        keys: List[str],
        measurement_name: str,
        result: str,
    ) -> None:
        """Initialise with class validation."""
        self.original = df
        self._keys = keys
        self._measurement_name = measurement_name
        self._result = result

        self._validate_class_inputs()

    def _validate_class_inputs(self) -> None:
        self._validate_dataframe()
        self._validate_keys()
        self._validate_measurement_name()
        self._validate_result()

    def _validate_dataframe(self) -> None:
        self._validate_dataframe_has_more_than_one_column()
        self._validate_dataframe_has_more_than_one_row()

    def _validate_dataframe_has_more_than_one_column(self) -> None:
        if len(self.original.columns) <= 1:
            raise ValueError("DataFrame has only one column.")

    def _validate_dataframe_has_more_than_one_row(self) -> None:
        if len(self.original) <= 1:
            raise ValueError("DataFrame has only one row.")

    def _validate_keys(self) -> None:
        self._validate_keys_list_is_not_empty()
        self._validate_keys_are_in_dataframe()

    def _validate_keys_list_is_not_empty(self) -> None:
        if not self._keys:
            raise ValueError("No keys provided to normalise on.")

    def _validate_keys_are_in_dataframe(self) -> None:
        for key in self._keys:
            if key not in self.original:
                raise ValueError("Keys provided are not in DataFrame.")

    def _validate_measurement_name(self) -> None:
        if not self._measurement_name:
            raise ValueError("Column name for measurement title not provided.")

    def _validate_result(self) -> None:
        if not self._result:
            raise ValueError("Column name for result not provided.")

    def normalise(self) -> None:
        """Assign one set of readings to each row."""
        self.normalised = self.original.pivot(
            index=self._keys,
            columns=self._measurement_name,
            values=self._result,
        )
