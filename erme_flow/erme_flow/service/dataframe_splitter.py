"""Module to hold Dataframe Splitter Class."""
from typing import Any, List

import pandas as pd


class DFSplitter:
    """Class to split DataFrames into DataFrames unique to sites.

    Parameters
    ----------
    src_df (pd.DataFrame)           DataFrame to be split
    key (str)                       Key upon which to split the DataFrame

    Attributes
    ----------
    original (pd.DataFrame)         Original DataFrame provided
    separated (List[pd.DataFrame])  Split out Dataframes

    Methods
    -------
    split                           Separate the DataFrame according to key
    """

    original: pd.DataFrame
    separated: List[pd.DataFrame]
    _key: str

    def __init__(self, src_df: pd.DataFrame, key: str) -> None:
        """Initialise class and validate inputs."""
        self.original = src_df
        self._key = key
        self._validate_class_inputs()

    def _validate_class_inputs(self) -> None:
        self._validate_dataframe()
        self._validate_columns()

    def _validate_dataframe(self) -> None:
        self._validate_columns_can_have_reference_and_measured_data()
        self._validate_there_are_at_least_three_datum()

    def _validate_columns_can_have_reference_and_measured_data(self) -> None:
        if len(self.original.columns) <= 1:
            raise ValueError("Insufficient data in frame.")

    def _validate_there_are_at_least_three_datum(self) -> None:
        if len(self.original) <= 2:
            raise ValueError("Insufficient data in frame.")

    def _validate_columns(self) -> None:
        self._validate_key_is_not_empty()
        self._validate_key_exists_in_dataframe()

    def _validate_key_is_not_empty(self) -> None:
        if not self._key:
            raise ValueError("No key provided to split on.")

    def _validate_key_exists_in_dataframe(self) -> None:
        if self._key not in self.original:
            raise ValueError("Provided key is not in DataFrame.")

    def split(self) -> List[pd.DataFrame]:
        """Split DataFrames into list of smaller DataFrames by key."""
        self.separated = []

        self._split_into_dataframes()
        self._reset_indexes_on_dataframes()
        self._drop_carried_over_index_from_original_dataframe()

        return self.separated

    def _split_into_dataframes(self) -> None:
        unique_rows: Any = self.original[self._key].unique()
        row: str

        for row in unique_rows:
            new_df: pd.DataFrame
            new_df = self.original[self.original[self._key] == row]

            self.separated.append(new_df)

    def _reset_indexes_on_dataframes(self) -> None:
        i: int
        df: pd.DataFrame

        for i, df in enumerate(self.separated):
            self.separated[i] = df.reset_index()

    def _drop_carried_over_index_from_original_dataframe(self) -> None:
        i: int
        df: pd.DataFrame

        for i, df in enumerate(self.separated):
            self.separated[i] = df.drop(columns="index")
