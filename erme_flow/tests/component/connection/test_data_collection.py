"""Test module to hold Data Collection Test class."""
import os
from typing import Any, Dict, List

import boto3
import pandas as pd
import pytest
from moto import mock_s3

from erme_flow.connection.data_collection import DataCollection as DataSyncS3


class TestDataSyncS3:
    """Test class to test Data Collection class."""

    data: Dict[str, List[Any]]
    df: pd.DataFrame
    file_for_upload: str
    upload_file_path: str

    def setup_method(self) -> None:
        """Set up test class."""
        os.environ["AWS_ACCESS_KEY_ID"] = "testing"
        os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
        os.environ["AWS_SECURITY_TOKEN"] = "testing"
        os.environ["AWS_SESSION_TOKEN"] = "testing"
        os.environ["AWS_DEFAULT_REGION"] = "us-east-1"

        self.data = {
            "name": [
                "Tom",
                "Harry",
            ],
            "age": [
                20,
                30,
            ]
        }
        self.df = pd.DataFrame(self.data)

        self.file_for_upload = "file_for_upload.csv"
        self.upload_file_path = f"tests/component/connection/" \
                                f"{self.file_for_upload}"

    def test_when_bucket_attribute_is_empty_then_error(self) -> None:
        """RBICEP: Error."""
        with pytest.raises(ValueError) as e_info:
            DataSyncS3(
                bucket="",
                file_name="data_file.csv",
            )

        assert e_info.value.args == ValueError("No S3 bucket specified.").args

    def test_when_file_name_attribute_is_empty_then_error(self) -> None:
        """RBICEP: Error."""
        with pytest.raises(ValueError) as e_info:
            DataSyncS3(
                bucket="test_bucket",
                file_name="",
            )

        assert e_info.value.args == ValueError("No file name specified.").args

    @mock_s3
    def test_when_bucket_does_not_exist_then_error(self) -> None:
        """RBICEP: Error."""
        with pytest.raises(ValueError) as e_info:
            DataSyncS3(
                bucket="test_bucket",
                file_name="data_file.csv",
            )

        assert e_info.value.args == ValueError("Bucket does not exist.").args

    @mock_s3
    def test_when_file_does_not_exist_in_bucket_then_error(self) -> None:
        """RBICEP: Error."""
        connection = boto3.resource("s3")
        connection.create_bucket(Bucket="test_bucket")

        collection = DataSyncS3(
            bucket="test_bucket",
            file_name="data_file.csv",
        )

        with pytest.raises(ValueError) as e_info:
            collection.retrieve()

        assert e_info.value.args == ValueError("File not in bucket.").args

    @mock_s3
    def test_when_object_retrieved_then_returns_dataframe(self) -> None:
        """RBICEP: Right."""
        connection = boto3.resource("s3")
        connection.create_bucket(Bucket="test_bucket")

        self.df.to_csv(self.upload_file_path)

        s3_client = boto3.client("s3")
        s3_client.upload_file(
            "tests/data/raw.csv",
            "test_bucket",
            "raw.csv",
        )

        collection = DataSyncS3(
            bucket="test_bucket",
            file_name="raw.csv",
        )
        retrieved_df = collection.retrieve()

        pd.testing.assert_frame_equal(
            left=retrieved_df,
            right=pd.read_csv("tests/data/raw.csv"),
        )

        os.remove(self.upload_file_path)

    @mock_s3
    def test_when_df_uploaded_then_object_is_csv(self) -> None:
        """RBICEP: Right."""
        connection = boto3.resource("s3")
        connection.create_bucket(Bucket="test_bucket")

        collection = DataSyncS3(
            bucket="test_bucket",
            file_name=self.file_for_upload,
        )
        collection.upload(self.df)

        s3_client = boto3.client("s3")
        s3_object = s3_client.get_object(
            Bucket="test_bucket",
            Key=self.file_for_upload,
        )

        uploaded_df = pd.read_csv(s3_object["Body"])
        uploaded_df = uploaded_df.drop(["Unnamed: 0"], axis=1)

        pd.testing.assert_frame_equal(uploaded_df, self.df)
