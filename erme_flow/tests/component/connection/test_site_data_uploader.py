"""Module to hold test class for Site Data Uploader."""
from math import nan
from typing import Any, Dict

import boto3
import pytest
from moto import mock_dynamodb

from erme_flow.connection.site_data_uploader import SiteDataUploader


class TestSiteDataUploader:
    """Test class for Site Data Uploader."""

    table: Dict[str, str]
    data: Dict[str, Any]
    site_data_uploader: SiteDataUploader

    def setup_method(self) -> None:
        """Set up test class."""
        self.table = {
            "table": "Site Data",
            "partition_key": "Site Name",
            "primary_key_in_data": "NAME",
        }
        self.data = {
            "PTCODE": 70920142,
            "NAME": "RIVER EIRGHT AT HORSESHOE FALLS",
            "PTTYPE_DESC": "FRESHWATER - RIVERS",
            "NGR": "SX6377258053",
            "LAST_SAMPLED": "07-Dec-18",
            "MATERIAL": "2AZZ",
            "MATL_DESC": "RIVER / RUNNING SURFACE WATER",
            "QUAL": nan
        }

        self.site_data_uploader = SiteDataUploader(
            table=self.table,
            data=self.data,
        )

    def test_when_data_dict_is_empty_then_error(self) -> None:
        """RBICEP: Error."""
        self.data = {}

        with pytest.raises(ValueError) as e_info:
            SiteDataUploader(
                table=self.table,
                data=self.data,
            )

        assert e_info.value.args == ValueError(
            "No data provided for upload."
        ).args

    def test_when_key_values_are_not_in_table_dict_then_error(self) -> None:
        """RBICEP: Error."""
        self.table = {}

        with pytest.raises(ValueError) as e_info:
            SiteDataUploader(
                table=self.table,
                data=self.data,
            )

        assert e_info.value.args == ValueError(
            "Key table values not provided for DynamoDB."
        ).args

    def test_when_partition_key_is_not_in_data_dict_then_error(self) -> None:
        """RBICEP: Error."""
        self.table["primary_key_in_data"] = "Forty-two"
        print(self.table)

        with pytest.raises(ValueError) as e_info:
            SiteDataUploader(
                table=self.table,
                data=self.data,
            )

        assert e_info.value.args == ValueError(
            "Primary key not found in data."
        ).args

    @mock_dynamodb
    def test_when_data_uploaded_then_table_contains_one_row(self) -> None:
        """RBICEP: Right."""
        connection = boto3.resource("dynamodb")
        table = connection.create_table(
            TableName="Site Data",
            KeySchema=[
                {
                    "AttributeName": "Site Name",
                    "KeyType": "HASH",
                },
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "Site Name",
                    "AttributeType": "S",
                },
            ],
            ProvisionedThroughput={
                "ReadCapacityUnits": 5,
                "WriteCapacityUnits": 5
            }
        )

        self.site_data_uploader.upload()

        assert table.scan()["Count"] == 1

    @mock_dynamodb
    def test_when_data_uploaded_then_item_contains_all_fields(self) -> None:
        """RBICEP: Right."""
        connection = boto3.resource("dynamodb")
        table = connection.create_table(
            TableName="Site Data",
            KeySchema=[
                {
                    "AttributeName": "Site Name",
                    "KeyType": "HASH",
                },
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": "Site Name",
                    "AttributeType": "S",
                },
            ],
            ProvisionedThroughput={
                "ReadCapacityUnits": 5,
                "WriteCapacityUnits": 5
            }
        )

        self.site_data_uploader.upload()
        uploaded_data = table.scan()["Items"][0]

        for key, value in self.data.items():
            assert key in uploaded_data
