"""Module to hold test class for Raw Data Consolidator interface."""
import boto3
import pandas as pd
import pytest
from moto import mock_dynamodb, mock_s3

from erme_flow.interface.raw_data_consolidator import RawDataConsolidator
from erme_flow.model import ConsolidationKeys
from erme_flow.model import RawDataConsolidatorBuckets as Buckets
from erme_flow.model import RawDataConsolidatorTableDetails as TableDetails


class TestRawDataConsolidator:
    """Test class for RawDataConsolidator class."""

    buckets: Buckets
    table_details: TableDetails
    consolidation_keys: ConsolidationKeys
    consolidator: RawDataConsolidator

    raw_df: pd.DataFrame
    raw_data_filename: str
    raw_data_file: str

    def setup_method(self) -> None:
        """Set up test class."""
        self.buckets = Buckets(
            raw_data="test-rawdata",
            processed_data="test-processeddata",
        )
        self.table_details = TableDetails(
            table="test-sitedata",
            partition_key="Site Name",
            primary_key_in_data="NAME",
        )
        self.consolidation_keys = ConsolidationKeys(
            splitting="NAME",
            normalisation=[
                "Date",
                "Time",
                "DoW",
            ],
            measured_entity="DETNAME",
            result="RESULT",
        )
        self.consolidator = RawDataConsolidator(
            buckets=self.buckets,
            table_details=self.table_details,
            consolidation_keys=self.consolidation_keys,
        )

        self.raw_data_filename = "raw.csv"
        self.raw_data_file = f"tests/data/{self.raw_data_filename}"
        self.raw_df = pd.read_csv(self.raw_data_file)

    def _create_aws_mocks(self) -> None:
        self._create_raw_bucket()
        self._create_dynamodb_table()
        self._create_processed_bucket()

    def _create_raw_bucket(self) -> None:
        s3 = boto3.resource("s3")
        s3.create_bucket(Bucket=self.buckets.raw_data)

        s3_client = boto3.client("s3")
        s3_client.upload_file(
            self.raw_data_file,
            self.buckets.raw_data,
            self.raw_data_filename,
        )

    def _create_processed_bucket(self) -> None:
        s3 = boto3.resource("s3")
        s3.create_bucket(Bucket=self.buckets.processed_data)

    def _create_dynamodb_table(self) -> None:
        dynamodb = boto3.resource("dynamodb")
        dynamodb.create_table(
            TableName=self.table_details.table,
            KeySchema=[
                {
                    "AttributeName": self.table_details.partition_key,
                    "KeyType": "HASH",
                },
            ],
            AttributeDefinitions=[
                {
                    "AttributeName": self.table_details.partition_key,
                    "AttributeType": "S",
                },
            ],
            ProvisionedThroughput={
                "ReadCapacityUnits": 5,
                "WriteCapacityUnits": 5
            }
        )

    def test_when_buckets_strings_are_empty_then_error(self) -> None:
        """RBICEP: Error."""
        self.buckets = Buckets(
            raw_data="",
            processed_data="",
        )
        with pytest.raises(ValueError) as e_info:
            RawDataConsolidator(
                buckets=self.buckets,
                table_details=self.table_details,
                consolidation_keys=self.consolidation_keys,
            )

        assert e_info.value.args == ValueError(
            "Bucket list contains empty values."
        ).args

    def test_when_table_details_are_empty_then_error(self) -> None:
        """RBICEP: Error."""
        self.table_details = TableDetails(
            table="",
            partition_key="",
            primary_key_in_data="",
        )
        with pytest.raises(ValueError) as e_info:
            RawDataConsolidator(
                buckets=self.buckets,
                table_details=self.table_details,
                consolidation_keys=self.consolidation_keys,
            )

        assert e_info.value.args == ValueError(
            "Table Details contain empty values."
        ).args

    @mock_dynamodb
    @mock_s3
    def test_when_run_then_processed_attr_has_two_aug_dfs(self) -> None:
        """RBICEP: Right."""
        self._create_aws_mocks()

        self.consolidator.run()

        assert len(self.consolidator.processed) == 2

    @mock_dynamodb
    @mock_s3
    def test_when_run_two_rows_are_uploaded_to_dynamodb(self) -> None:
        """RBICEP: Right."""
        self._create_aws_mocks()

        self.consolidator.run()

        dynamodb = boto3.resource("dynamodb")
        table = dynamodb.Table(self.table_details.table)

        assert table.scan()["Count"] == 2

    @mock_dynamodb
    @mock_s3
    def test_when_run_two_files_are_uploaded_to_s3(self) -> None:
        """RBICEP: Right."""
        self._create_aws_mocks()

        self.consolidator.run()

        s3 = boto3.client("s3")
        file_count: int = len(s3.list_objects(
            Bucket=self.buckets.processed_data,
        )["Contents"])

        assert file_count == 2

    @mock_dynamodb
    @mock_s3
    def test_when_run_then_dict_of_two_frames_produced(self) -> None:
        """RBICEP: Right."""
        self._create_aws_mocks()

        assert len(self.consolidator.run()) == 2
