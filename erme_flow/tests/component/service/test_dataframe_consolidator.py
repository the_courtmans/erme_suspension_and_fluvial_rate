"""Module to hold Test class for DataFrame Consolidator."""
import pandas as pd
import pytest

from erme_flow.model import ConsolidationKeys
from erme_flow.service.dataframe_consolidator import DFConsolidator


class TestDFConsolidator:
    """Test class for DataFrame consolidator."""

    df: pd.DataFrame
    keys: ConsolidationKeys
    consolidator: DFConsolidator

    def setup_method(self) -> None:
        """Set up test class."""
        self.df = pd.read_csv("tests/data/raw.csv")
        self.keys = ConsolidationKeys(
            splitting="NAME",
            normalisation=[
                "Date",
                "Time",
                "DoW",
            ],
            measured_entity="DETNAME",
            result="RESULT",
        )

        self.consolidator = DFConsolidator(
            df=self.df,
            keys=self.keys,
        )

    def test_when_df_has_too_few_rows_then_error(self) -> None:
        """RBICEP: Error."""
        self.df = pd.DataFrame()

        with pytest.raises(ValueError) as e_info:
            DFConsolidator(
                df=self.df,
                keys=self.keys,
            )

        assert e_info.value.args == ValueError(
            "DataFrame has too few rows."
        ).args

    def test_when_no_keys_in_key_object_then_error(self) -> None:
        """RBICEP: Error."""
        self.keys = ConsolidationKeys(
            splitting="",
            normalisation=[],
            measured_entity="",
            result="",
        )

        with pytest.raises(ValueError) as e_info:
            DFConsolidator(
                df=self.df,
                keys=self.keys,
            )

        assert e_info.value.args == ValueError(
            "Keys object contains empty values."
        ).args

    def test_when_keys_not_in_df_then_error(self) -> None:
        """RBICEP: Error."""
        self.keys = ConsolidationKeys(
            splitting="Image",
            normalisation=[
                "DataFrame",
                "not",
                "SQL",
            ],
            measured_entity="STIENCE",
            result="Thing Value",
        )

        with pytest.raises(ValueError) as e_info:
            DFConsolidator(
                df=self.df,
                keys=self.keys,
            )

        assert e_info.value.args == ValueError(
            "Keys do not exist in DataFrame."
        ).args

    def test_when_consolidated_then_right_frames_count_produced(self) -> None:
        """RBICEP: Right."""
        self.consolidator.consolidate()

        assert len(self.consolidator._split_frames) == 2

    def test_when_id_extracted_then_right_frames_count_produced(self) -> None:
        """RBICEP: Right."""
        self.consolidator.consolidate()

        assert len(self.consolidator._augmented_frames) == 2

    def test_when_normalised_then_dict_with_names_produced(self) -> None:
        """RBICEP: Right."""
        self.consolidator.consolidate()

        for name in [
            "RIVER EIRGHT RIVENDELL (RIVER ABSTRACTION POINT)",
            "RIVER EIRGHT AT HORSESHOE FALLS"
        ]:
            assert name in self.consolidator.processed
