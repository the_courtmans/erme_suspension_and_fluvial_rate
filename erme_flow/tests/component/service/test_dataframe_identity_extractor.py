"""Module to hold DataFrame Identity Extractor test class."""
from typing import Any, Dict

import pandas as pd
import pytest

from erme_flow.model import AugmentedDF
from erme_flow.service.dataframe_identity_extractor import DFIDExtractor


class TestDFIDExtractor:
    """Test class to test DFIDExtractor."""

    data: Dict[str, Any]
    df: pd.DataFrame
    df_id_extracting: DFIDExtractor

    def setup_method(self) -> None:
        """Set up test class."""
        self.data = {
                "Name": [
                    "Jersey",
                    "Jersey",
                ],
                "Country": [
                    "UK",
                    "UK",
                ],
                "Sheep": [
                    412,
                    572,
                ]
            }
        self.df = pd.DataFrame(self.data)
        self.df_id_extracting = DFIDExtractor(
            df=self.df,
        )
        self.augmented_df = AugmentedDF(
            id={
                "Name": "Jersey",
                "Country": "UK",
            },
            df=pd.DataFrame(
                {
                    "Sheep": [
                        412,
                        572,
                    ]
                }
            )
        )

    def test_when_all_columns_are_singly_valued_then_error(self) -> None:
        """RBICEP: Error."""
        self.data["Sheep"] = [1024, 1024]
        self.df = pd.DataFrame(self.data)

        with pytest.raises(ValueError) as e_info:
            DFIDExtractor(self.df)

        assert e_info.value.args == ValueError("All columns contain single va"
                                               "lues.").args

    def test_when_no_columns_are_singly_valued_then_error(self) -> None:
        """RBICEP: Error."""
        self.data["Name"] = ["Jersey", "Isle of Wight"]
        self.data["Country"] = ["UK", "India"]
        self.df = pd.DataFrame(self.data)

        with pytest.raises(ValueError) as e_info:
            DFIDExtractor(self.df)

        assert e_info.value.args == ValueError("No column contains single val"
                                               "ues.").args

    def test_when_data_extracted_then_correct_metadata_created(self) -> None:
        """RBICEP: Right."""
        self.df_id_extracting.extract()

        assert self.df_id_extracting.processed.id == self.augmented_df.id

    def test_when_metadata_extracted_then_dataframe_is_trimmed(self) -> None:
        """RBICEP: Right."""
        self.df_id_extracting.extract()

        pd.testing.assert_frame_equal(
            left=self.df_id_extracting.processed.df,
            right=self.augmented_df.df
        )
