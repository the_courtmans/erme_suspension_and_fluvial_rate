"""Module to hold DataFrame Normaliser test class."""
import pandas as pd
import pytest

from erme_flow.service.dataframe_identity_extractor import DFIDExtractor
from erme_flow.service.dataframe_normaliser import DFNormaliser
from erme_flow.service.dataframe_splitter import DFSplitter


class TestDataFrameNormaliser:
    """Test class for DataFrame Normaliser."""

    df: pd.DataFrame

    def setup_method(self) -> None:
        """Set up test class."""
        raw_df = pd.read_csv("tests/data/raw.csv")

        splitter = DFSplitter(raw_df, "NAME")
        first_df = splitter.split()[0]

        id_extractor = DFIDExtractor(first_df)
        self.df = id_extractor.extract().df

        self.keys = ["Date", "Time", "DoW"]
        self.measurement_name = "DETNAME"
        self.result = "RESULT"

        self.normaliser = DFNormaliser(
            df=self.df,
            keys=self.keys,
            measurement_name=self.measurement_name,
            result=self.result,
        )

    def test_when_src_df_has_one_column_then_error(self) -> None:
        """RBICEP: Error."""
        self.df = pd.DataFrame(
            {
                "Name": [
                    "Jeff",
                    "Geoff",
                ]
            }
        )

        with pytest.raises(ValueError) as e_info:
            DFNormaliser(
                df=self.df,
                keys=self.keys,
                measurement_name=self.measurement_name,
                result=self.result,
            )

        assert e_info.value.args == ValueError(
            "DataFrame has only one column."
        ).args

    def test_when_src_df_has_one_row_then_error(self) -> None:
        """RBICEP: Error."""
        self.df = pd.DataFrame(
            {
                "Name": ["Jeff"],
                "Age": [28],
            }
        )

        with pytest.raises(ValueError) as e_info:
            DFNormaliser(
                df=self.df,
                keys=self.keys,
                measurement_name=self.measurement_name,
                result=self.result,
            )

        assert e_info.value.args == ValueError(
            "DataFrame has only one row."
        ).args

    def test_when_keys_is_empty_then_error(self) -> None:
        """RBICEP: Error."""
        self.keys = []

        with pytest.raises(ValueError) as e_info:
            DFNormaliser(
                df=self.df,
                keys=self.keys,
                measurement_name=self.measurement_name,
                result=self.result,
            )

        assert e_info.value.args == ValueError(
            "No keys provided to normalise on."
        ).args

    def test_when_keys_not_in_dataframe_then_error(self) -> None:
        """RBICEP: Error."""
        self.keys = ["Shoe Size"]

        with pytest.raises(ValueError) as e_info:
            DFNormaliser(
                df=self.df,
                keys=self.keys,
                measurement_name=self.measurement_name,
                result=self.result,
            )

        assert e_info.value.args == ValueError(
            "Keys provided are not in DataFrame."
        ).args

    def test_when_measurement_name_is_empty_then_error(self) -> None:
        """RBICEP: Error."""
        self.measurement_name = ""

        with pytest.raises(ValueError) as e_info:
            DFNormaliser(
                df=self.df,
                keys=self.keys,
                measurement_name=self.measurement_name,
                result=self.result,
            )

        assert e_info.value.args == ValueError(
            "Column name for measurement title not provided."
        ).args

    def test_when_result_is_empty_then_error(self) -> None:
        """RBICEP: Error."""
        self.result = ""

        with pytest.raises(ValueError) as e_info:
            DFNormaliser(
                df=self.df,
                keys=self.keys,
                measurement_name=self.measurement_name,
                result=self.result,
            )

        assert e_info.value.args == ValueError(
            "Column name for result not provided."
        ).args

    def test_when_normalised_then_correct_number_or_rows(self) -> None:
        """RBICEP: Right."""
        self.normaliser.normalise()

        assert len(self.normaliser.normalised) == 16

    def test_when_normalised_then_all_measurement_names_in_df(self) -> None:
        """RBICEP: Right."""
        column_count: int = self.df["DETNAME"].nunique()

        self.normaliser.normalise()

        assert column_count == len(self.normaliser.normalised.columns)
