"""Module to hold Dataframe Splitter Test Class."""
import pandas as pd
import pytest

from erme_flow.service.dataframe_splitter import DFSplitter


class TestDFSplitter:
    """Test class to test DFSplitter."""

    df: pd.DataFrame
    key: str
    df_splitting: DFSplitter

    def setup_method(self) -> None:
        """Set up test class."""
        self.df = pd.DataFrame(
            {
                "Name": [
                    "Jersey",
                    "Jersey",
                    "Guernsey",
                ],
                "Sheep": [
                    412,
                    572,
                    20,
                ]
            }
        )
        self.key = "Name"
        self.df_splitting = DFSplitter(
            src_df=self.df,
            key=self.key
        )

    def test_when_df_has_fewer_than_two_columns_then_error(self) -> None:
        """RBICEP: Error."""
        self.df = pd.DataFrame()

        with pytest.raises(ValueError) as e_info:
            DFSplitter(self.df, self.key)

        assert e_info.value.args == ValueError(
            "Insufficient data in frame."
        ).args

    def test_when_df_has_fewer_than_three_rows_then_error(self) -> None:
        """RBICEP: Error."""
        self.df = self.df[:-1]

        with pytest.raises(ValueError) as e_info:
            DFSplitter(self.df, self.key)

        assert e_info.value.args == ValueError(
            "Insufficient data in frame."
        ).args

    def test_when_key_is_blank_then_error(self) -> None:
        """RBICEP: Error."""
        self.key = ""

        with pytest.raises(ValueError) as e_info:
            DFSplitter(
                src_df=self.df,
                key=self.key
            )

        assert e_info.value.args == ValueError(
            "No key provided to split on."
        ).args

    def test_when_key_is_not_in_dataframe_then_error(self) -> None:
        """RBICEP: Error."""
        self.key = "River Basins"

        with pytest.raises(ValueError) as e_info:
            DFSplitter(
                src_df=self.df,
                key=self.key
            )

        assert e_info.value.args == ValueError(
            "Provided key is not in DataFrame."
        ).args

    def test_when_split_then_right_number_of_dataframes_result(self) -> None:
        """RBICEP: Right."""
        self.df_splitting.split()

        assert len(self.df_splitting.separated) == 2

    def test_when_split_then_each_dfs_index_starts_at_zero(self) -> None:
        """RBICEP: Right."""
        self.df_splitting.split()

        for df in self.df_splitting.separated:
            assert df.first_valid_index() == 0

    def test_when_split_then_each_dfs_index_count_match_length(self) -> None:
        """RBICEP: Right."""
        self.df_splitting.split()

        for df in self.df_splitting.separated:
            assert df.last_valid_index() == len(df) - 1

    def test_when_split_then_extra_index_column_does_not_exist(self) -> None:
        """RBICEP: Right."""
        self.df_splitting.split()

        for df in self.df_splitting.separated:
            assert "index" not in df
