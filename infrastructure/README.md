# AWS CDK Infrastructure Code
> Infrastructure for Erme Suspension and Fluvial Rate project

## Getting Started
### Prerequisites
* [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) with configured profile

### Deploying environment
To deploy, run the following:
```bash
export AWS_PROFILE=<profile>
export AWS_NAMESPACE=<your name>  # this value should be "live" for live deployment

make deploy
```

## Design Notes
![Infrastructure](../img/infrastructure.png "Infrastructure")