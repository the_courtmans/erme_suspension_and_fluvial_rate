#!/usr/bin/env python3
import os

import aws_cdk as cdk

from infrastructure.infrastructure_stack import InfrastructureStack

ORGANISATION: str = "Courtman"
PROJECT: str = "ErmeSuspensionAndFluvialRate"
REGION: str
SAFE_DELETE: bool

if "AWS_NAMESPACE" in os.environ:
    AWS_NAMESPACE: str = os.environ["AWS_NAMESPACE"]
else:
    raise Exception("AWS_NAMESPACE must be set")

if AWS_NAMESPACE == "live":
    REGION = "Live"
    SAFE_DELETE = False
else:
    REGION = "Dev"
    SAFE_DELETE = True


app = cdk.App()
infrastructure = InfrastructureStack(
    scope=app,
    construct_id="InfrastructureStack",
    project=f"{ORGANISATION.lower()}-{PROJECT.lower()}",
    deploy_region=REGION.lower(),
    namespace=AWS_NAMESPACE,
    safe_delete=SAFE_DELETE,
)

cdk.Tags.of(infrastructure).add("Organisation", ORGANISATION)
cdk.Tags.of(infrastructure).add("Project", PROJECT)
cdk.Tags.of(infrastructure).add("Environment", REGION)

app.synth()
