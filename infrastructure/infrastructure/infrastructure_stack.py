"""Module to hold main stack."""

from aws_cdk import RemovalPolicy, Stack, aws_dynamodb, aws_s3
from constructs import Construct


class InfrastructureStack(Stack):
    """Stack holding main infrastructure."""

    project: str
    deploy_region: str
    namespace: str
    safe_delete: bool
    raw_data_bucket: aws_s3.Bucket
    processed_data_bucket: aws_s3.Bucket
    site_data_table: aws_dynamodb.Table

    def __init__(
            self,
            scope: Construct,
            construct_id: str,
            project: str,
            deploy_region: str,
            namespace: str,
            safe_delete: bool,
            **kwargs
    ) -> None:
        """Initialise stack with scope and namespace."""
        self.deploy_region = deploy_region
        super().__init__(scope, f"{construct_id}-{self.deploy_region}-"
                                f"{namespace}", **kwargs)

        self.project = project
        self.safe_delete = safe_delete
        self.namespace = namespace

        self.raw_data_bucket = self._create_bucket("raw data")
        self.processed_data_bucket = self._create_bucket("processed data")

        self.site_data_table = self._create_table(
            table_name="site data",
            partition_key="Site Name",
        )

    def _create_bucket(self, bucket_name: str) -> aws_s3.Bucket:
        bucket: aws_s3.Bucket = aws_s3.Bucket(
            self,
            id=bucket_name.replace(" ", "-"),
            bucket_name=self._name_generator(
                bucket_name.replace(" ", "")
            ),
            removal_policy=RemovalPolicy.DESTROY,
            auto_delete_objects=self.safe_delete,
        )

        return bucket

    def _create_table(
            self,
            table_name: str,
            partition_key: str
    ) -> aws_dynamodb.Table:
        table: aws_dynamodb.Table = aws_dynamodb.Table(
            self,
            id=table_name.replace(" ", "-"),
            table_name=self._name_generator(
                table_name.replace(" ", "")
            ),
            partition_key=aws_dynamodb.Attribute(
               name=partition_key,
               type=aws_dynamodb.AttributeType.STRING
            ),
            table_class=aws_dynamodb.TableClass.STANDARD_INFREQUENT_ACCESS,
            removal_policy=RemovalPolicy.DESTROY,
        )

        return table

    def _name_generator(self, identifier: str) -> str:
        name: str

        if self.deploy_region == "live":
            name = f"{self.project}-{identifier}"
        else:
            name = f"{self.project}-{self.deploy_region}-" \
                          f"{identifier}-{self.namespace}"

        return name
