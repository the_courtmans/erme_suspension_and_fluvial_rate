# Smoke Tests
> Smoke test package for testing the Erme Flow package

## Usage
1. Deploy test environment
1. Upload raw data csv to raw data bucket
1. Run smoke-tests package
   1. Run consolidator
   1. Test dataframe csv files exist as expected in processed data bucket
   1. Test rows appear in DynamoDB table
1. Destroy test environment

```bash
make deploy
make test
make destroy
```
