#!/usr/bin/env python3
import os

import aws_cdk as cdk

from infrastructure.infrastructure_stack import InfrastructureStack

ORGANISATION: str = "Courtman"
PROJECT: str = "ErmeSuspensionAndFluvialRate"
AWS_NAMESPACE: str = "test"
REGION: str = "Dev"
SAFE_DELETE: bool = True


app = cdk.App()
infrastructure = InfrastructureStack(
    scope=app,
    construct_id="InfrastructureStack",
    project=f"{ORGANISATION.lower()}-{PROJECT.lower()}",
    deploy_region=REGION.lower(),
    namespace=AWS_NAMESPACE,
    safe_delete=SAFE_DELETE,
)

cdk.Tags.of(infrastructure).add("Organisation", ORGANISATION)
cdk.Tags.of(infrastructure).add("Project", PROJECT)
cdk.Tags.of(infrastructure).add("Environment", REGION)

app.synth()
