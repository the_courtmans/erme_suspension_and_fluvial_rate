"""Test module for Consolidation class."""
import boto3
import pandas as pd
from erme_flow import (ConsolidationBuckets, ConsolidationDynamoDB,
                       ConsolidationKeys, Consolidator)
from erme_flow.model import AugmentedDF

data_file_path: str = "data/raw.csv"
data_file_on_s3: str = "raw.csv"
raw_data_bucket: str = "courtman-ermesuspensionandfluvialrate-dev-rawdata-test"
processed_data_bucket: str = "courtman-ermesuspensionandfluvialrate-dev-proc" \
                             "esseddata-test"
site_table: str = "courtman-ermesuspensionandfluvialrate-dev-sitedata-test"


def setup_module() -> None:
    """Upload raw data to bucket."""
    s3_client = boto3.client("s3")
    s3_client.upload_file(
        data_file_path,
        raw_data_bucket,
        data_file_on_s3,
    )


def test_consolidation_pathway() -> None:
    run_consolidation()

    count: int = 0

    s3 = boto3.resource("s3")
    bucket = s3.Bucket(processed_data_bucket)
    for item in bucket.objects.all():
        count += 1

    assert count == 2

    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table(site_table)

    assert table.scan()["Count"] == 2


def run_consolidation() -> None:
    keys = ConsolidationKeys(
        splitting="NAME",
        normalisation=[
            "Date",
            "Time",
            "DoW",
        ],
        measured_entity="DETNAME",
        result="RESULT",
    )
    buckets = ConsolidationBuckets(
        raw_data=raw_data_bucket,
        processed_data=processed_data_bucket,
    )
    table_details = ConsolidationDynamoDB(
        table=site_table,
        partition_key="Site Name",
        primary_key_in_data="NAME",
    )
    consolidator = Consolidator(
        buckets=buckets,
        table_details=table_details,
        consolidation_keys=keys,
    )
    consolidator.run()
